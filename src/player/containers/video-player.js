import React, { Component } from 'react';
import VideoPlayerLayout from '../components/video-player-layout';
import Video from '../components/video';
import Title from '../components/title';
import PlayPause from '../components/play-pause';
import Timer from '../components/timer';
import Controls from '../components/video-player-controls';
import { formmattedTime } from '../../utilities/formatter-time';
import ProgressBar from '../components/progress-bar';
import Spinner from '../components/spinner';
import Volumen from '../components/volumen';
import FullScrenn from '../components/full-screen';

class VideoPlayer extends Component{
	state = {

		pause:true,
		duration:0,
		durationFloat:0,
		currentTimeFloat:0,
		loading:false,
		volume:1,
		lastVolumeState:null,
	}

	togglePlay = () =>{
		this.setState({
			pause:!this.state.pause
		})
	}

	componentDidMount(){
		this.setState({
			pause: (!this.props.autoplay)
		})
	}

	handleLoadedMetadata = event =>{
		this.video = event.target;
		this.setState({
			duration: formmattedTime( this.video.duration ),
			durationFloat:this.video.duration,
		})
	}

	handleTimeUpdate = event =>{
		this.setState({
			currentTime: formmattedTime( this.video.currentTime ),
			currentTimeFloat: this.video.currentTime 
		})		
	}

	handleProgressChange = event =>{
		this.video.currentTime = event.target.value
	}

	handleSeeking = event =>{
		this.setState({
			loading:true
		})

	}
	handleSeeked = event =>{
		this.setState({
			loading:false
		})
	}

	handleVolumeChange = event =>{
		this.video.volume = event.target.value
	}



	  mute = ()  => {
	    const lastState = this.video.volume
	    this.setState({
	      lastVolumeState: lastState,
	      volume: 0
	    })
	    this.video.volume = 0
	  }

	  unmute = () => {
	    this.setState({
	      volume: this.state.lastVolumeState
	    })
	    this.video.volume = this.state.lastVolumeState
	  }


	hadleVolumeToggle = event =>{


	    this.video.volume !== 0 ? this.mute() : this.unmute()

	}

	handelFullScreenClick = event =>{
		console.log("Hola");
		if(!document.webkitIsFullScreen){
			this.player.webkitRequestFullscreen();
		}else{
			document.exitFullscreen();

		}
	}

	setRef = element=>{
		this.player=element
	}

	render(){
		return(
			<VideoPlayerLayout setRef={this.setRef}>
				<Title
					title={this.props.title}
				/>

				<Controls>
					<PlayPause
						pause 		=	{this.state.pause}
						handleClick	=	{this.togglePlay}
					/>
					<Timer
						duration 	= {this.state.duration}
						currentTime = {this.state.currentTime}
					/>

					<ProgressBar
						duration 			 = {this.state.durationFloat}
						value 	 			 = {this.state.currentTimeFloat}
						handleProgressChange = {this.handleProgressChange}
					/>
					<Volumen
						handleVolumeChange={this.handleVolumeChange}
						hadleVolumeToggle = {this.hadleVolumeToggle}
						value={this.state.volume}

					/>

					<FullScrenn
						handelFullScreenClick={this.handelFullScreenClick}
					 />
				</Controls>

				<Spinner
					active = {this.state.loading}
				/>
				

				<Video
					
					autoplay={this.state.autoplay}
					pause={this.state.pause}
					handleLoadedMetadata = {this.handleLoadedMetadata}
					handleTimeUpdate = {this.handleTimeUpdate}
					handleSeeking = {this.handleSeeking}
					handleSeeked = {this.handleSeeked}
					src={this.props.src}
				/>
			</VideoPlayerLayout>
		)

	}
}
export default VideoPlayer;