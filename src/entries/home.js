import React from 'react';
import { render }  from 'react-dom';
//import Playlist from './src/playlist/components/playlist'; //Despues de haber tenido el componente creado
import Home from '../pages/containers/home';
import data from '../api.json';


const homeContainer = document.getElementById('home-container');
//const holaMundo = <h1>Hola mundoooo!</h1>;
//ReactDom.render(LoQueSeRenderiza, DondeSeRenderiza)//un componente, lugar en el dom
render(<Home data={data}/>,homeContainer); //Para este caso se hizo uso no de un compoente si no un elemento de HTML
