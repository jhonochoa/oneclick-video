import React from 'react'
import './search.css';
//function Search(porps) {
//	return(

//	)
//}



const Search = (props) =>(
	
	<form 
		className="Search"
		onSubmit={props.handleSubmit}
	>
		<input
			ref={props.setRef} 
			className="Search-input"
			placeholder="Busca un video"
			type="text"
			name="search"
			onChange = {props.handleChange}
			value={props.value}
		/>
	</form>

)

export default Search;