import React from 'react';
import Media from './media.js';
import './playlist.css';

function Playlist(props){

	return(
		<div className = "Playlist">

			{
				props.playlist.map((item) => { //por cada elemento de la playlist se envia un media
					return <Media openModal = {props.handleOpenModal} {...item} key={item.id} />
				})
			}
					
		</div>
	)
	
}

export default Playlist;