import React, { PureComponent } from 'react'; //Para crear un componete siempre va esste import
import PropTypes from 'prop-types';
import './media.css'


class Media extends PureComponent{ //Una clase represeta un componente
  //Metodo que tienen todas las clases, que se ejcuta al ser instanciada
  /*constructor(props){
    super(props)
    this.state = {
      author:props.author
    }

    //this.hanledClick = this.hanledClick.bind(this); //Se enlaza el evento del click con la clase Media
  }*/
  state = {
    author : 'Jhon M. Ochoa Lemus'
  }
  //De esta manera hereda el contexto del padre

  handleClick = (event) =>{
    this.props.openModal(this.props);
  }


  render(){//Contiene toda la forma del componente, el html(No es html es JSX, ES UNA SINTAXIS PARA CONSTRUIR ELEMENTOS DE React )
    const styles = { //Variable para definir el estilo, // Atributo que contiene el estilo
      container: { //Los estilos son como un json
        Color: '#44546b',
        cursor: 'pointer',
        width: 260,
        border: '1px solid red'
      }
    } 
    //Se pudede agregar em linea la constante usada <div style =  {styles.container}>  
    return (
      <div className = "Media" onClick={this.handleClick}>  
        <div className = "Media-cover">
          <img
            src={this.props.cover}
            alt=""
            width={260}
            height={160}
            className = "Media-image"
          />
          <h3 className="Media-title">{this.props.title}</h3>
          <p className="Media-author">{this.props.author}</p>
        </div>
      </div>
    )
  }
}
//Para validar los valores que llegan a las propiedades
Media.propTypes = {
  cover:  PropTypes.string,
  title:  PropTypes.string.isRequired,
  author: PropTypes.string,
  type:   PropTypes.oneOf(['video', 'audio'])
}
export default Media; //Se exporta el compoente para ser usado
